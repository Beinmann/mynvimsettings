require("lgrue")

-- local vimrc = "./vimrc.vim"
-- vim.cmd.source(vimrc) 

local config_dir = vim.fn.stdpath('config')
local vim_file_path = config_dir .. "/vimscript/lgrue/remaps.vim"
vim.cmd("source " .. vim_file_path)
vim_file_path = config_dir .. "/vimscript/lgrue/autocmd.vim"
vim.cmd("source " .. vim_file_path)
vim_file_path = config_dir .. "/vimscript/lgrue/todo_highlighter.vim"
vim.cmd("source " .. vim_file_path)
