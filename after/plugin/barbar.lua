vim.g.barbar_auto_setup = false -- disable auto-setup

require'barbar'.setup {
    focus_on_close = 'previous',
    insert_at_end = true,
}
