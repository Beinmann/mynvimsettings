" With help from chatGPT
" TodoHighlighter is a function you can call which will color all marked of
" todos (- [x]) green. and the function ApplyGreenText() will mark all lines
" below marked off checkboxes. But you don't have to call ApplyGreenText()
" manually each time because call TodoHighlighter() will add an autocmd call
" to it on every insertleave
" These functions are optional and I plan on using them only in some sessions
function! ApplyGreenText()
  let l:lineNum = 1
  let l:applyGreen = 0
  let l:baseIndent = 0
  while l:lineNum <= line('$')
    let l:lineText = getline(l:lineNum)
    if l:applyGreen == 0 && l:lineText =~ '^\s*- \[x\]'
      let l:baseIndent = indent(l:lineNum)
      let l:applyGreen = 1
    elseif l:applyGreen == 1
      if indent(l:lineNum) > l:baseIndent
        " Apply green text to this line
        call matchadd('TodoCheckedContinue', '\%'.l:lineNum.'l.*')
      else
        let l:applyGreen = 0
      endif
    endif
    let l:lineNum += 1
  endwhile
endfunction

function! TodoHighlighter()
    syntax match TodoCheckedStart "\s*- \[x\].*"
    highlight TodoCheckedStart ctermfg=green guifg=Green
    highlight TodoCheckedContinue ctermfg=green guifg=Green
    call ApplyGreenText()
    autocmd InsertLeave * call ApplyGreenText()
endfunction

function! TodoHighlight()
    call TodoHighlighter()
endfunction
