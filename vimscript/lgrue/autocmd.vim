autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent! loadview

" Copied from ChatGPT
" Function to source local init.vim
function! SourceLocalInitVim()
  if filereadable('./init.vim')
    execute 'source' './init.vim'
  endif
endfunction

" Auto-command to run the function when Neovim starts
autocmd VimEnter * call SourceLocalInitVim()

augroup LocListMapping
    autocmd!
    autocmd FileType qf nnoremap <buffer> l :lnext<CR><C-W><C-P>lh
    autocmd FileType qf nnoremap <buffer> h :lprev<CR><C-W><C-P>lh
augroup END





function! OpenNetrwIfBuffer()
    if bufname('%') == ''
        Ex
    endif
endfunction

" Execute the function on Vim startup
autocmd VimEnter * call OpenNetrwIfBuffer()












