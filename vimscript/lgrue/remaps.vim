" Additional things loaded in init.lua
" This is great because I can use the standard .vim format
" but also lua

" TODO get this to work somehow (a very similar try from me is in the
" remap.lua file



"""""""""""" Not sure if I keep these """"""""""""

" H and L go to the start/end of the visual Line
nnoremap H g0
nnoremap L g$

inoremap jk <Esc>


" Remap C to be equal to deleting the current char and then inserting
nnoremap C xi

" Swap the roles of Ctrl + R and Ctrl + V around for more intuitive bindings
" in windows
" Exception in normal mode since Ctrl + V for visual block selection is really
" nice
inoremap <C-V> <C-R>
inoremap <C-R> <C-V>

cnoremap <C-V> <C-R>
cnoremap <C-R> <C-V>

" Quickly switch back and forth between the most recent buffers
nnoremap <leader>- <C-^>
 









"""""""""""" Remaps that are here to stay (for now) """"""""""""



" x doesn't yank text
nnoremap x "_x


" From ChatGPT
function! ToggleVSplitOrNextTab()
    if winnr('$') == 1
        execute 'vsplit'
        execute 'wincmd w'
    else
        execute 'wincmd w'
    endif
endfunction
nnoremap <Tab> :call ToggleVSplitOrNextTab()<CR>


" TODO put these remaps, etc. into a folder that makes more sense
" Remaps for working with buffers
nnoremap <Leader>bl :ls<CR>:b<Space>
nnoremap <leader>bb :enew<CR>













